package com.boot.base.handler;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-02 15:06
 */
@RestControllerAdvice
public class HandlerExe {
    /**
     * 异常信息
     * {"msg":"参数ID错误","code":"500","url":"http://localhost:8082/boot/base/0"}
     * @since 2023-07-02 15:08
     */
    @ExceptionHandler(value = Exception.class)
    public Map<String,String> handler02 (HttpServletRequest request, Exception e){
        var errorMap = new HashMap<String,String>() ;
        errorMap.put("code","500");
        errorMap.put("url",request.getRequestURL().toString());
        errorMap.put("msg",e.getMessage());
        return errorMap ;
    }
}
