package com.boot.base.web;

import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-02 17:40
 */
@RestController
public class BootLogWeb {

    // 日志打印
    private static final Logger LOGGER = LoggerFactory.getLogger(BootBaseWeb.class);

    /**
     * 日志依赖
     * 【spring-boot-starter-web】
     * 【spring-boot-starter】
     * 【spring-boot-starter-logging】
     * 【logback-classic】
     * @since 2023-07-02 17:49
     */
    @GetMapping("/boot/print/log")
    public String printLog (HttpServletRequest request){
        LOGGER.info("remote-host:{}",request.getRemoteHost());
        LOGGER.info("request-uri:{}",request.getRequestURI());
        return request.getServerName() ;
    }
}
