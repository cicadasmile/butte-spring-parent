# butte-spring

## 1、案例简介

SpringBoot3框架基础和进阶用法，与数据源和中间件的集成，比如连接池，缓存，消息队列，搜索引擎，安全管理，定时任务等。

## 2、参考文章

- [SpringBoot3基础用法](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B01、基础案例.md)
- [SpringBoot3进阶用法](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B02、进阶案例.md)
- [SpringBoot3之Web编程](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B03、Web编程.md)
- [SpringBoot3文件管理](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B04、文件管理.md)
- [SpringBoot3数据库集成](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B05、数据库集成.md)
- [SpringBoot3分库分表](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B06、分库分表.md)
- [SpringBoot3集成PostgreSQL](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B14、PostgreSQL.md)
- [SpringBoot3安全管理](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B07、安全管理.md)
- [SpringBoot3集成Redis](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B08、Redis缓存.md)
- [SpringBoot3集成ElasticSearch](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B09、ES搜索引擎.md)
- [SpringBoot3集成Quartz](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B10、Quartz任务.md)
- [SpringBoot3集成RocketMq](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B11、RocketMQ消息.md)
- [SpringBoot3集成Kafka](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B12、Kafka消息.md)
- [SpringBoot3集成ZooKeeper](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/boot3/B13、ZooKeeper组件.md)

## 3、仓库整合

| 仓库 | 描述 |
|:---|:---|
| [butte-java](https://gitee.com/cicadasmile/butte-java-note) |Java编程文档整理，基础、架构，大数据 |
| [butte-frame](https://gitee.com/cicadasmile/butte-frame-parent) |微服务组件，中间件，常用功能二次封装 |
| [butte-flyer](https://gitee.com/cicadasmile/butte-flyer-parent) |butte-frame二次浅封装，实践案例 |
| [butte-auto](https://gitee.com/cicadasmile/butte-auto-parent) |Jenkins+Docker+K8S实现自动化持续集成 |