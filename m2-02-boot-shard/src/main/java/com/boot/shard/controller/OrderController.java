package com.boot.shard.controller;

import com.boot.shard.entity.Order;
import com.boot.shard.mapper.BuyerMapper;
import com.boot.shard.mapper.OrderMapper;
import com.boot.shard.mapper.SellerMapper;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 订单控制层
 * @author 公众号:知了一笑
 * @since 2023-07-19 11:32
 */
@RestController
public class OrderController {

    @Resource
    private BuyerMapper buyerMapper ;
    @Resource
    private SellerMapper sellerMapper ;
    @Resource
    private OrderMapper orderMapper ;

    /**
     * 查询订单详情
     * GET: http://localhost:8080/order/info/100
     * {
     *     "seller":{
     *         "id":3,
     *         "sellerName":"Three卖家"
     *     },
     *     "order":{
     *         "orderId":100,
     *         "sellerId":3,
     *         "buyerId":1
     *     },
     *     "buyer":{
     *         "id":1,
     *         "buyerName":"买家One"
     *     }
     * }
     */
    @GetMapping("/order/info/{orderId}")
    public Map<String,Object> orderInfo (@PathVariable Integer orderId){
        Map<String,Object> orderMap = new HashMap<>() ;
        Order order = orderMapper.selectByPrimaryKey(orderId) ;
        if (order != null){
            orderMap.put("order",order) ;
            orderMap.put("buyer",buyerMapper.selectByPrimaryKey(order.getBuyerId())) ;
            orderMap.put("seller",sellerMapper.selectByPrimaryKey(order.getSellerId())) ;
        }
        return orderMap ;
    }
}
