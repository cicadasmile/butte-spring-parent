CREATE TABLE `tb_order_0` (
  `order_id` int(11) NOT NULL COMMENT '订单ID',
  `seller_id` int(11) NOT NULL COMMENT '卖家ID',
  `buyer_id` int(11) NOT NULL COMMENT '买家ID',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

CREATE TABLE `tb_order_1` (
  `order_id` int(11) NOT NULL COMMENT '订单ID',
  `seller_id` int(11) NOT NULL COMMENT '卖家ID',
  `buyer_id` int(11) NOT NULL COMMENT '买家ID',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

CREATE TABLE `tb_order_2` (
  `order_id` int(11) NOT NULL COMMENT '订单ID',
  `seller_id` int(11) NOT NULL COMMENT '卖家ID',
  `buyer_id` int(11) NOT NULL COMMENT '买家ID',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';