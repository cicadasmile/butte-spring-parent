CREATE TABLE `tb_order` (
  `order_id` int(11) NOT NULL COMMENT '订单ID',
  `seller_id` int(11) NOT NULL COMMENT '卖家ID',
  `buyer_id` int(11) NOT NULL COMMENT '买家ID',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

CREATE TABLE `tb_buyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `buyer_name` varchar(30) NOT NULL COMMENT '买家名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='买家表';

CREATE TABLE `tb_seller` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `seller_name` varchar(30) NOT NULL COMMENT '卖家名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='卖家表';

INSERT INTO `shard_db`.`tb_buyer` (`id`, `buyer_name`) VALUES (1, '买家One');
INSERT INTO `shard_db`.`tb_buyer` (`id`, `buyer_name`) VALUES (2, '买家Two');
INSERT INTO `shard_db`.`tb_buyer` (`id`, `buyer_name`) VALUES (3, 'Three买家');

INSERT INTO `shard_db`.`tb_seller` (`id`, `seller_name`) VALUES (1, '卖家One');
INSERT INTO `shard_db`.`tb_seller` (`id`, `seller_name`) VALUES (2, '卖家Two');
INSERT INTO `shard_db`.`tb_seller` (`id`, `seller_name`) VALUES (3, 'Three卖家');