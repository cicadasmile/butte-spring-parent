package com.boot.shard;

import com.boot.shard.entity.Order;
import com.boot.shard.entity.OrderExample;
import com.boot.shard.mapper.OrderMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 分库分表测试
 * @author 公众号:知了一笑
 * @since 2023-07-19 10:24
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ShardTest {

    @Autowired
    private OrderMapper orderMapper ;

    /**
     * 写入100条数据
     * SQL: db_0 ::: insert into tb_order_1 (order_id, seller_id, buyer_id) values (?, ?, ?) ::: [100, 2, 2]
     * db_${order_id % 2} 100%2=0 即 db_0
     * tb_order_${order_id % 3} 100%3=1 即 tb_order_1
     * 验证分库分表
     * -- db_0+db_1 六张表汇总
     * SELECT * FROM (
     * 	-- db_0 三张表汇总
     * 	SELECT t1.*,"db_0" DB,"tb_order_0" TB FROM shard_db_0.tb_order_0 t1 UNION ALL
     * 	SELECT t2.*,"db_0" DB,"tb_order_1" TB FROM shard_db_0.tb_order_1 t2 UNION ALL
     * 	SELECT t3.*,"db_0" DB,"tb_order_2" TB FROM shard_db_0.tb_order_2 t3 UNION ALL
     * 	-- db_1 三张表汇总
     * 	SELECT t4.*,"db_1" DB,"tb_order_0" TB FROM shard_db_1.tb_order_0 t4 UNION ALL
     * 	SELECT t5.*,"db_1" DB,"tb_order_1" TB FROM shard_db_1.tb_order_1 t5 UNION ALL
     * 	SELECT t6.*,"db_1" DB,"tb_order_2" TB FROM shard_db_1.tb_order_2 t6
     * ) tb1 ORDER BY tb1.order_id ASC
     */
    @Test
    public void testOrderInsert (){
        for (int i=1 ; i<= 100 ; i++){
            Order order = new Order(i,i%3+1,i%3+1) ;
            // orderMapper.insert(order) ;
        }
    }

    /**
     * SQL: db_1 ::: select order_id, seller_id, buyer_id from tb_order_2 where order_id = ? ::: [5]
     * db_${order_id % 2} 5%2=1 即 db_1
     * tb_order_${order_id % 3} 5%3=2 即 tb_order_2
     */
    @Test
    public void testOrderQuery (){
        Order order = orderMapper.selectByPrimaryKey(5) ;
        System.out.println(order);
    }

    /**
     * SQL: db_0 ::: update tb_order_1 set seller_id = ?,buyer_id = ? where order_id = ? ::: [3, 1, 100]
     * order_id=100 路由到：db_0.tb_order_1
     */
    @Test
    public void testOrderUpdate (){
        Order order = orderMapper.selectByPrimaryKey(100) ;
        if (order != null){
            // 原数据：买家和卖家ID都是2
            order.setBuyerId(1);
            order.setSellerId(3);
            orderMapper.updateByPrimaryKey(order) ;
        }
    }

    /**
     * 查询所有的分库和分表，最后汇总的计算结果
     */
    @Test
    public void testOrderPage (){
        // 1、设置分页和查询条件
        PageHelper.startPage(1,10) ;
        OrderExample orderExample = new OrderExample() ;
        orderExample.createCriteria().andBuyerIdEqualTo(2).andSellerIdEqualTo(2);
        orderExample.setOrderByClause("order_id desc");
        // 2、查询数据
        List<Order> orderList = orderMapper.selectByExample(orderExample) ;
        // 3、构建分页实体对象
        PageInfo<Order> pageInfo = new PageInfo<>(orderList) ;
        System.out.println(pageInfo);
    }
}
