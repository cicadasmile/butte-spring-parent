package com.boot.zkeeper;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.locks.InterProcessReadWriteLock;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.nio.charset.StandardCharsets;

/**
 * @author 公众号:知了一笑
 * @since 2024-01-20 14:52
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ConfigTest {

    @Autowired
    private CuratorFramework client ;

    @Test
    public void testCreate () throws Exception {
        // 创建一个持久化节点，断开连接时不会自动删除
        client.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT).forPath("/path1");
    }

    @Test
    public void testExists () throws Exception {
        // 判断节点是否存在，path2不存在所以stat2是null
        Stat stat1 = client.checkExists().forPath("/path1");
        System.out.println(stat1);
        Stat stat2 = client.checkExists().forPath("/path2");
        System.out.println(stat2);
    }

    @Test
    public void testSetData () throws Exception {
        // 设置节点数据
        client.setData().forPath("/path1", "data1".getBytes(StandardCharsets.UTF_8));
    }

    @Test
    public void testCreateAndSet () throws Exception {
        // 创建一个持久化节点并设置节点数据
        client.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                .forPath("/path3","data3".getBytes(StandardCharsets.UTF_8));
    }

    @Test
    public void testGetData () throws Exception {
        // 查询节点数据
        byte[] data = client.getData().forPath("/path3");
        System.out.println(new String(data,StandardCharsets.UTF_8));
    }

    @Test
    public void testDelete () throws Exception {
        // 删除节点
        client.delete().guaranteed().deletingChildrenIfNeeded().forPath("/path3");
    }

    @Test
    public void testReadLock () throws Exception {
        // 读写锁-读
        InterProcessReadWriteLock lock = new InterProcessReadWriteLock(client,"/lock-read");
        lock.readLock().acquire();
        System.out.println("获取-ReadLock");
        lock.readLock().release();
    }

    @Test
    public void testWriteLock () throws Exception {
        // 读写锁-写
        InterProcessReadWriteLock lock = new InterProcessReadWriteLock(client,"/lock-write");
        lock.writeLock().acquire();
        System.out.println("获取-WriteLock");
        lock.writeLock().release();
    }
}