package com.boot.file.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import java.util.Arrays;
import java.util.List;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-15 09:57
 */
public class DataExtVO {

    @ExcelProperty("城市编号")
    private Integer cityId ;

    @ExcelProperty("城市名称")
    private String cityName ;

    public static List<DataExtVO> getSheetList (){
        return Arrays.asList(
                new DataExtVO(1,"北京"),
                new DataExtVO(2,"上海"),
                new DataExtVO(3,"广州")
                );
    }

    public DataExtVO() {
    }

    public DataExtVO(Integer cityId, String cityName) {
        this.cityId = cityId;
        this.cityName = cityName;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}