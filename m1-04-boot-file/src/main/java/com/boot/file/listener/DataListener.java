package com.boot.file.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.boot.file.entity.DataVO;
import java.util.Map;

/**
 * Excel数据解析监听器
 * @author 公众号:知了一笑
 * @since 2023-07-15 09:59
 */
public class DataListener extends AnalysisEventListener<DataVO> {

    @Override
    public void invoke(DataVO data, AnalysisContext context) {
        System.out.println("DataListener："+data);
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("DataListener："+headMap);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        System.out.println("DataListener：after...all...analysed");
    }
}