package com.boot.web.controller;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Date;

@Schema(description = "ParamBO实体类")
public class ParamBO {

    @Schema(description = "ID")
    private Integer id ;

    @Schema(description = "名称")
    private String name ;

    @Schema(description = "时间")
    private Date date ;

    @Schema(description = "备注",hidden = true)
    private String remark ;

    public ParamBO() {
    }

    public ParamBO(Integer id, String name, Date date) {
        this.id = id;
        this.name = name;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}