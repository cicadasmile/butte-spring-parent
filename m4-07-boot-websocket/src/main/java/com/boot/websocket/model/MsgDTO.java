package com.boot.websocket.model;

import lombok.Data;

/**
 * @author 知了一笑
 * @date 2024-05-04 17:00
 */
@Data
public class MsgDTO {

    private String userId;

    private String msg;

    private String msgType;
}
