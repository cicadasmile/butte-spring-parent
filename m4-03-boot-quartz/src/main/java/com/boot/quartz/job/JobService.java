package com.boot.quartz.job;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-26 11:43
 */
public interface JobService {

    void run(String params);

}
