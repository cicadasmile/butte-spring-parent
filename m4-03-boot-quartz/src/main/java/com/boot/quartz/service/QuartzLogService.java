package com.boot.quartz.service;

import com.boot.quartz.entity.QuartzLog;
import com.boot.quartz.mapper.QuartzLogMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author 公众号:知了一笑
 * @since 2023-07-26 11:02
 */
@Service("quartzLogService")
public class QuartzLogService {

    @Resource
    private QuartzLogMapper quartzLogMapper ;

    public Integer insert(QuartzLog quartzLog) {
        return quartzLogMapper.insert(quartzLog);
    }
}
