package com.boot.quartz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.quartz.entity.QuartzJob;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface QuartzJobMapper extends BaseMapper<QuartzJob> {

}