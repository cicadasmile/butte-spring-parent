package com.boot.search.mapper;

import com.boot.search.entity.Contents;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 内容表 Mapper 接口
 */
public interface ContentsMapper extends BaseMapper<Contents> {

}
