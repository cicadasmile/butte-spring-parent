package com.boot.search;

import com.boot.search.service.ContentsIndexService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 
 * @author 公众号:知了一笑
 * @since 2023-07-28 14:39
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ContentsIndexServiceTest {

    @Autowired
    private ContentsIndexService contentsIndexService ;

    @Test
    public void initIndex (){
        contentsIndexService.initIndex();
    }

}
