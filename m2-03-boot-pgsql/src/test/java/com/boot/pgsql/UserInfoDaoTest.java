package com.boot.pgsql;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.pgsql.dao.UserInfoDao;
import com.boot.pgsql.entity.UserInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserInfoDaoTest {

    @Autowired
    private UserInfoDao userInfoDao;

    @Test
    public void testCrud() {
        UserInfo userInfo = userInfoDao.getById(1);
        System.out.println(userInfo.getUserName()+";"+userInfo.getMobile());

        userInfo.setUpdateTime(new Date());
        userInfo.setUpdaterId(23);
        userInfoDao.updateById(userInfo);

        UserInfo newUser = new UserInfo() ;
        newUser.setUserName("Test用户");
        newUser.setPassWord("123456");
        newUser.setGender(1);
        newUser.setStatus(1);
        newUser.setMobile("18888888888");
        newUser.setCreateTime(new Date());
        newUser.setCreatorId(10);
        userInfoDao.save(newUser);
    }

    @Test
    public void testPage () {

        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>() ;
        queryWrapper.orderByAsc(UserInfo::getId) ;

        System.out.println("===selectPage===1=======================");
        IPage<UserInfo> page1 = new Page<>(1,2) ;
        IPage<UserInfo> userPage1 = userInfoDao.page(page1,queryWrapper);
        if (userPage1.getSize() > 0){
            userPage1.getRecords().forEach(System.out::println);
        }

        System.out.println("===selectPage===2=======================");
        IPage<UserInfo> page2 = new Page<>(2,2) ;
        IPage<UserInfo> userPage2 = userInfoDao.page(page2,queryWrapper);
        if (userPage2.getSize() > 0){
            userPage2.getRecords().forEach(System.out::println);
        }
    }
}