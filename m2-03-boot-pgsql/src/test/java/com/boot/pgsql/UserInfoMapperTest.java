package com.boot.pgsql;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.pgsql.entity.UserModel;
import com.boot.pgsql.mapper.UserInfoMapper;
import jakarta.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 知了一笑
 * @date 2024-03-24 11:22
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserInfoMapperTest {

    @Resource
    private UserInfoMapper userInfoMapper;

    @Test
    public void testPage () {

        System.out.println("===selectPage===1=======================");
        IPage<UserModel> page1 = new Page<>(1,2) ;
        IPage<UserModel> userPage1 = userInfoMapper.userModelPage(page1);
        if (userPage1.getSize() > 0){
            userPage1.getRecords().forEach(System.out::println);
        }

        System.out.println("===selectPage===2=======================");
        IPage<UserModel> page2 = new Page<>(2,2) ;
        IPage<UserModel> userPage2 = userInfoMapper.userModelPage(page2);
        if (userPage2.getSize() > 0){
            userPage2.getRecords().forEach(System.out::println);
        }
    }
}
