CREATE TABLE "public"."user_info" (
                                      "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
                                          INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1
),
   "user_name" varchar(32) COLLATE "pg_catalog"."default" NOT NULL,
   "pass_word" varchar(100) COLLATE "pg_catalog"."default",
   "mobile" varchar(100) COLLATE "pg_catalog"."default",
   "gender" int4,
   "status" int4,
   "creator_id" int4,
   "create_time" timestamp(6),
   "updater_id" int4,
   "update_time" timestamp(6),
   CONSTRAINT "user_info_pkey" PRIMARY KEY ("id")
)
;

ALTER TABLE "public"."user_info"
    OWNER TO "postgres";

COMMENT ON COLUMN "public"."user_info"."id" IS '注解ID';

COMMENT ON COLUMN "public"."user_info"."user_name" IS '用户名';

COMMENT ON COLUMN "public"."user_info"."pass_word" IS '密码';

COMMENT ON COLUMN "public"."user_info"."mobile" IS '手机号';


CREATE TABLE "public"."sys_user" (
    "id" int4 NOT NULL,
    "user_name" varchar(32) COLLATE "pg_catalog"."default" NOT NULL,
    CONSTRAINT "sys_user_pkey" PRIMARY KEY ("id")
)
;

ALTER TABLE "public"."sys_user"
    OWNER TO "postgres";