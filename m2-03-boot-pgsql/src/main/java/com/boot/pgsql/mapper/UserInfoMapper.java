package com.boot.pgsql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.boot.pgsql.entity.UserInfo;
import com.boot.pgsql.entity.UserModel;

/**
 * @author 知了一笑
 * @date 2024-03-23 21:43
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    IPage<UserModel> userModelPage(IPage<UserModel> page);

}
