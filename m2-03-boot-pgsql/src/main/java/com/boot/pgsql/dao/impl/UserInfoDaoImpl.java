package com.boot.pgsql.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boot.pgsql.dao.UserInfoDao;
import com.boot.pgsql.entity.UserInfo;
import com.boot.pgsql.mapper.UserInfoMapper;
import org.springframework.stereotype.Service;

/**
 * @author 知了一笑
 * @date 2024-03-24 10:36
 */
@Service
public class UserInfoDaoImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoDao {

}
