package com.boot.pgsql.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boot.pgsql.entity.UserInfo;

/**
 * @author 知了一笑
 * @date 2024-03-24 10:33
 */
public interface UserInfoDao extends IService<UserInfo> {

}
