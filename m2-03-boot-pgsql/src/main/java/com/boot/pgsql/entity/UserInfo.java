package com.boot.pgsql.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.io.Serializable;
import java.util.Date;

/**
 * @author 知了一笑
 * @since 2023-07-16 13:16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@TableName("user_info")
public class UserInfo implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String userName;

    private String passWord;

    private String mobile;

    private Integer gender;

    private Integer status;

    private Integer creatorId;

    private Date createTime;

    private Integer updaterId;

    private Date updateTime;

}
