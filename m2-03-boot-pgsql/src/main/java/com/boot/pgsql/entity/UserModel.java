package com.boot.pgsql.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author 知了一笑
 * @date 2024-03-24 11:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserModel {
    private Integer userId ;
    private String userName ;
    private String createTime ;
    private Integer creatorId ;
    private String creatorName ;
}
