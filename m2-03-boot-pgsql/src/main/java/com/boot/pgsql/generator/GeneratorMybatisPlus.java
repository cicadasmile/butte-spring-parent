package com.boot.pgsql.generator;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * @author 知了一笑
 * @date 2024-03-24 11:31
 */
public class GeneratorMybatisPlus {

    private static final String jdbcUrl = "数据库地址";
    private static final String outDir = "存放路径";

    public static void main(String[] args) {
        // 数据源配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig.Builder
                (jdbcUrl,"postgres","postgres")
                .build();

        // 代码生成器
        AutoGenerator autoGenerator = new AutoGenerator(dataSourceConfig);

        // 全局配置
        GlobalConfig globalConfig = new GlobalConfig.Builder()
                .outputDir(outDir).disableOpenDir().author("知了一笑") // .enableSwagger()
                .build();

        // 分包配置
        PackageConfig packageConfig = new PackageConfig.Builder()
                .parent("com.boot.pgsql.generator").controller("controller")
                .service("dao").serviceImpl("dao.impl").mapper("mapper").entity("entity")
                .build();

        // 策略配置
        StrategyConfig strategyConfig = new StrategyConfig.Builder()
                .addInclude("user_info","sys_user")
                .addTablePrefix("")
                .entityBuilder().enableLombok()
                .naming(NamingStrategy.underline_to_camel)
                .columnNaming(NamingStrategy.underline_to_camel)
                .controllerBuilder().formatFileName("%sController")
                .entityBuilder().formatFileName("%s")
                .serviceBuilder().formatServiceFileName("%sDao").formatServiceImplFileName("%sDaoImpl")
                .mapperBuilder().formatMapperFileName("%sMapper").formatXmlFileName("%sMapper")
                .build();

        autoGenerator.global(globalConfig);
        autoGenerator.packageInfo(packageConfig);
        autoGenerator.strategy(strategyConfig);

        // 执行
        autoGenerator.execute();
    }
}
