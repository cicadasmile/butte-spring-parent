package com.boot.redis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-21 13:37
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdValue {

    private Integer id ;

    private Object value ;
}
