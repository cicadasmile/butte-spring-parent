package com.boot.redis.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 内容信息表
 * @author 公众号:知了一笑
 * @since 2023-07-21 17:18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_article")
public class Article implements Serializable {

    private static final long serialVersionUID=1L;

    public Article(String title, String detail, Date createTime) {
        this.title = title;
        this.detail = detail;
        this.createTime = createTime;
    }

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 主题
     */
    private String title;

    /**
     * 详细信息
     */
    private String detail;

    /**
     * 创建时间
     */
    private Date createTime;

}
