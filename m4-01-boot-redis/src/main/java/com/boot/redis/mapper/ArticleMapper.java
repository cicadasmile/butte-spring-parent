package com.boot.redis.mapper;

import com.boot.redis.cache.MybatisCache;
import com.boot.redis.entity.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 内容信息表 Mapper 接口
 * @author 公众号:知了一笑
 * @since 2023-07-21 16:52
 */
@CacheNamespace(implementation = MybatisCache.class)
public interface ArticleMapper extends BaseMapper<Article> {

}
