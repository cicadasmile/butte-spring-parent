package com.boot.redis;

import com.boot.redis.service.RedisDataService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author 公众号:知了一笑
 * @since 2023-07-21 13:46
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RedisDataTest {

    @Autowired
    private RedisDataService redisDataService ;

    @Test
    public void testString () throws Exception{
        redisDataService.dataString();
    }

    @Test
    public void testList () {
        redisDataService.dataList();
    }

    @Test
    public void testSet () {
        redisDataService.dataSet();
    }

    @Test
    public void testHash () {
        redisDataService.dataHash();
    }

    @Test
    public void testSortedSet () {
        redisDataService.dataSortedSet();
    }
}
