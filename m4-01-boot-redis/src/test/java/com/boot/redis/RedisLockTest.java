package com.boot.redis;

import com.boot.redis.service.LockRegistryService;
import com.boot.redis.service.RedisLockService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.concurrent.TimeUnit;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-21 18:32
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RedisLockTest {

    @Autowired
    private RedisLockService redisLockService ;
    @Autowired
    private StringRedisTemplate stringRedisTemplate ;

    /**
     * 输出信息
     * order-id:6778
     * lock...try...fail [打印四次获取锁失败]
     * lock...try...success
     */
    @Test
    public void testLock () throws Exception {
        String orderLock = "order-id:6778";
        Boolean lockFlag = redisLockService.lock(orderLock,10);
        if (lockFlag){
            String lockKey = stringRedisTemplate.opsForValue().get(orderLock);
            System.out.println(lockKey);
            while (true){
                TimeUnit.SECONDS.sleep(2);
                Boolean lockTry = redisLockService.lock(orderLock,10);
                if (lockTry){
                    System.out.println("lock...try...success");
                    break ;
                } else {
                    System.out.println("lock...try...fail");
                }
            }
        }
    }

    @Autowired
    private LockRegistryService lockRegistryService ;

    /**
     * 输出信息
     * lockKey:c66d1b38-32e9-4cb8-a5ea-3d62509ac267
     * lockFlag1:true；lockFlag2：true
     */
    @Test
    public void testReentrant (){
        // 可重入锁测试
        String orderLock = "order-id:3579";
        Boolean lockFlag1 = lockRegistryService.tryLock(orderLock, 10L);
        if (lockFlag1){
            String lockKey = stringRedisTemplate.opsForValue().get("REDIS-LOCK:"+orderLock);
            System.out.println("lockKey:"+lockKey);
        }
        Boolean lockFlag2 = lockRegistryService.tryLock(orderLock, 5L);
        // 两次结果都是True
        System.out.println("lockFlag1:"+lockFlag1+"；lockFlag2："+lockFlag2);
        // 释放锁
        lockRegistryService.unlock(orderLock);
    }

    /**
     * 输出信息
     * lockKey:45f49b66-fc24-4268-ba28-aa48eec7ef1f
     * key:order-id:6879;lock:true
     */
    @Test
    public void testReentrantLock01 (){
        String orderLock = "order-id:6879";
        Boolean lockFlag1 = lockRegistryService.tryLock(orderLock, 5L);
        if (lockFlag1){
            String lockKey = stringRedisTemplate.opsForValue().get("REDIS-LOCK:"+orderLock);
            System.out.println("lockKey:"+lockKey);
        }
        System.out.println("key:"+orderLock+";lock:"+lockFlag1);
    }

    /**
     * 输出信息
     * lock...try...first...fail
     * lock...try...second...success
     */
    @Test
    public void testReentrantLock02 (){
        // 可重入锁测试
        String orderLock = "order-id:6879";
        Boolean lockFlag1 = lockRegistryService.reTryLock(orderLock, 5L,1);
        if (lockFlag1){
            System.out.println("lock...try...first...success");
        } else {
            System.out.println("lock...try...first...fail");
            Boolean lockFlag2 = lockRegistryService.reTryLock(orderLock, 5L,1);
            if (lockFlag2){
                System.out.println("lock...try...second...success");
            } else {
                System.out.println("lock...try...second...fail");
            }
        }
    }
}
