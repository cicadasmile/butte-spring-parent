package com.boot.security.config;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-23 13:47
 */
public class WhiteConfig {

    public static String[] whiteList (){
        return new String[]{"/login","/register","/swagger-ui/**","/v3/api-docs/**"};
    }

}
