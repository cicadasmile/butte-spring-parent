package com.boot.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 配置文件
 * @author 公众号:知了一笑
 * @since 2023-07-11 19:09
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

}