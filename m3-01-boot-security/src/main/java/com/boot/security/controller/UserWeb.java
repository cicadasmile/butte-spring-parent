package com.boot.security.controller;

import com.boot.security.entity.UserBase;
import com.boot.security.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 用户接口
 * @author 公众号:知了一笑
 * @since 2023-07-22 18:31
 */
@RestController
public class UserWeb {

    @Resource
    private UserService userService ;

    @PostMapping("/register")
    public String register (@RequestBody UserBase userBase){
        return "register-"+userService.register(userBase) ;
    }

    @GetMapping("/select/{id}")
    public UserBase select (@PathVariable Integer id){
        return userService.getById(id) ;
    }

    @PreAuthorize("hasRole('User')")
    @GetMapping("/user/{id}")
    public UserBase getUser (@PathVariable Integer id){
        return userService.getById(id) ;
    }

    @PreAuthorize("hasRole('Admin')")
    @GetMapping("/admin/{id}")
    public UserBase getAdmin (@PathVariable Integer id){
        return userService.getById(id) ;
    }

    @PreAuthorize("hasAnyRole('User','Admin')")
    @GetMapping("/query/{id}")
    public UserBase query (@PathVariable Integer id){
        return userService.getById(id) ;
    }
}
