package com.boot.security.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author 公众号:知了一笑
 * @since 2023-07-23 09:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rep<T> {

    private int code ;

    private String msg ;

    private T data ;

    public Boolean isSus (){
        return code == 200 ;
    }

    /**
     * 构造方法
     */
    public Rep(int code, String msg ) {
        this.code = code;
        this.msg = msg;
    }
}