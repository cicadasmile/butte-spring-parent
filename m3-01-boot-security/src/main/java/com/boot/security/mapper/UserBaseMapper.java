package com.boot.security.mapper;

import com.boot.security.entity.UserBase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户基础表 Mapper 接口
 * @author 公众号:知了一笑
 * @since 2023-07-22 18:29
 */
public interface UserBaseMapper extends BaseMapper<UserBase> {

}
