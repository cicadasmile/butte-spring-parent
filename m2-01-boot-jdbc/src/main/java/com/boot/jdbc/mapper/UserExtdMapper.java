package com.boot.jdbc.mapper;

import com.boot.jdbc.entity.UserExtd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户扩展信息 Mapper 接口
 * @author 公众号:知了一笑
 * @since 2023-07-16 19:31
 */
public interface UserExtdMapper extends BaseMapper<UserExtd> {

}
