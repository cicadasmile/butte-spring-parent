package com.boot.jdbc.dao.impl;

import com.boot.jdbc.entity.UserExtd;
import com.boot.jdbc.mapper.UserExtdMapper;
import com.boot.jdbc.dao.UserExtdDao;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 用户扩展信息 服务实现类
 * @author 公众号:知了一笑
 * @since 2023-07-16 19:42
 */
@Service
public class UserExtdDaoImpl extends ServiceImpl<UserExtdMapper, UserExtd> implements UserExtdDao {

}
