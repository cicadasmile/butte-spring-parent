package com.boot.jdbc.dao.impl;

import com.boot.jdbc.entity.User;
import com.boot.jdbc.mapper.UserMapper;
import com.boot.jdbc.dao.UserDao;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 用户基础信息 服务实现类
 * @author 公众号:知了一笑
 * @since 2023-07-16 19:42
 */
@Service
public class UserDaoImpl extends ServiceImpl<UserMapper, User> implements UserDao {

}
