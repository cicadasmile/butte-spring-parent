package com.boot.jdbc.entity;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-16 18:06
 */
@Data
@ToString
public class UserModel {

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 邮件
     */
    private String email;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 城市名称
     */
    private String cityName;

    /**
     * 学校名称
     */
    private String school;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 状态：1启用，2删除
     */
    private Integer state;
}
