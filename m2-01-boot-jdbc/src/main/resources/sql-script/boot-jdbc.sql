CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_name` varchar(30) NOT NULL COMMENT '用户名称',
  `email` varchar(50) DEFAULT NULL COMMENT '邮件',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `state` int(1) DEFAULT '1' COMMENT '状态：1启用，2删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户基础信息';

CREATE TABLE `tb_user_extd` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `city_name` varchar(50) DEFAULT NULL COMMENT '城市名称',
  `school` varchar(200) DEFAULT NULL COMMENT '学校名称',
  PRIMARY KEY (`id`),
  KEY `user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户扩展信息';

INSERT INTO `boot-jdbc`.`tb_user` (`id`, `user_name`, `email`, `phone`, `create_time`, `update_time`, `state`) VALUES (1, '张三', 'zhangsan@qq.com', '18923459687', '2023-07-16 15:56:05', '2023-07-16 15:56:07', 1);
INSERT INTO `boot-jdbc`.`tb_user` (`id`, `user_name`, `email`, `phone`, `create_time`, `update_time`, `state`) VALUES (2, '李四', 'lisi@qq.com', '18923459679', '2023-07-16 15:56:05', '2023-07-16 15:56:07', 1);
INSERT INTO `boot-jdbc`.`tb_user` (`id`, `user_name`, `email`, `phone`, `create_time`, `update_time`, `state`) VALUES (3, '王五', 'wangwu@qq.com', '18923459636', '2023-07-16 15:56:05', '2023-07-16 15:56:07', 1);

INSERT INTO `boot-jdbc`.`tb_user_extd` (`id`, `user_id`, `city_name`, `school`) VALUES (1, 1, '北京', '北京大学');
INSERT INTO `boot-jdbc`.`tb_user_extd` (`id`, `user_id`, `city_name`, `school`) VALUES (2, 2, '上海', '上海大学');
INSERT INTO `boot-jdbc`.`tb_user_extd` (`id`, `user_id`, `city_name`, `school`) VALUES (3, 3, '广州', '广州大学');