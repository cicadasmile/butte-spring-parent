package com.boot.jdbc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.jdbc.entity.User;
import com.boot.jdbc.entity.UserModel;
import com.boot.jdbc.mapper.UserMapper;
import jakarta.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Mapper测试
 * @author 公众号:知了一笑
 * @since 2023-07-16 19:34
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserMapperTest {

    @Resource
    private UserMapper userMapper ;

    @Test
    public void testQuery (){
        // 1、主键查询
        System.out.println("===userOne=============================================");
        User userOne = userMapper.selectById(1) ;
        System.out.println(userOne);
        // 2、查询全部
        System.out.println("===userList=============================================");
        List<User> userList = new LambdaQueryChainWrapper<>(userMapper).list();
        userList.forEach(System.out::println);
        // 3、查询指定ID
        System.out.println("===userIdList=============================================");
        List<User> userIdList = userMapper.selectBatchIds(Arrays.asList(3,4));
        userIdList.forEach(System.out::println);
        // 4、查询指定字段
        System.out.println("===userColumnsList=============================================");
        List<User> userColumnsList = new LambdaQueryChainWrapper<>(userMapper)
                .select(User::getUserName,User::getPhone,User::getEmail)
                .like(User::getPhone,"189")
                .orderByDesc(User::getId).last("limit 2").list();
        userColumnsList.forEach(System.out::println);
    }

    @Test
    public void testPage (){
        // 1、分页查询
        System.out.println("===selectPage=============================================");
        IPage<User> userPage = new Page<>(2,2) ;
        IPage<User> userPageList = userMapper.selectPage(userPage,new QueryWrapper<>());
        userPageList.getRecords().forEach(System.out::println);

        // 2、自定义查询分页
        System.out.println("===queryUserPage=============================================");
        IPage<UserModel> userModelPage = userMapper.queryUserPage(userPage);
        userModelPage.getRecords().forEach(System.out::println);
    }

    @Test
    public void testInsert (){
        // 1、新增数据
        List<User> userBatch = Arrays.asList(
                new User(null,"Zhang三","Zhang@qq.com","18623459687",new Date(),new Date(),1),
                new User(null,"Li四","Li@qq.com","18623459689",new Date(),new Date(),1),
                new User(null,"Wang五","Wang@qq.com","18623459635",new Date(),new Date(),1));
        userBatch.forEach(userMapper::insert);
    }

    @Test
    public void testUpdate (){
        // 1、修改数据
        User user = userMapper.selectById(7);
        user.setState(2);
        userMapper.updateById(user);
    }

    @Test
    public void testDelete (){
        // 1、删除数据
        userMapper.deleteById(7);
    }
}
