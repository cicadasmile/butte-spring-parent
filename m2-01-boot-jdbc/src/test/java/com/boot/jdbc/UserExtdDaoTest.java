package com.boot.jdbc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.jdbc.dao.UserExtdDao;
import com.boot.jdbc.entity.UserExtd;
import jakarta.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-16 19:49
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserExtdDaoTest {

    @Resource
    private UserExtdDao userExtdDao ;

    @Test
    public void testUserExtdDao (){
        // 1、分页查询
        IPage<UserExtd> page = new Page<>();
        page.setCurrent(1);
        page.setSize(2);
        IPage<UserExtd> userExtdPage = userExtdDao.page(page,new QueryWrapper<>());
        List<UserExtd> userExtdList = userExtdPage.getRecords();
        userExtdList.forEach(System.out::println);
    }
}
