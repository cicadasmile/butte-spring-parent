package com.boot.jdbc;

import com.boot.jdbc.entity.User;
import com.boot.jdbc.service.JdbcService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Date;
import java.util.List;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-16 16:03
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JdbcServiceTest {

    @Autowired
    private JdbcService jdbcService ;

    @Test
    public void testAdd (){
        User user = new User() ;
        user.setUserName("赵六");
        user.setEmail("zhaoliu@qq.com");
        user.setPhone("18923459672");
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        System.out.println("testAdd："+jdbcService.addData(user));
    }

    @Test
    public void testQuery (){
        List<User> userList = jdbcService.queryAll() ;
        userList.forEach(System.out::println);
    }

    @Test
    public void testUpdate (){
        System.out.println("testUpdate："+jdbcService.updateName(4,"赵六(ZL)"));
    }

    @Test
    public void testDelete (){
        System.out.println("testDelete："+jdbcService.deleteId(4));
    }
}
